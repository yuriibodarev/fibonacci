## Fibonacci
We want a fibonnacci server that calculates numbers for us.

elixir
iex> Fibonacci.get_number(0)
{:ok, 0}
iex> Fibonacci.get_number(100)
{:ok, 354224848179261915075}
iex> Fibonacci.get_number(1)
{:ok, 1}
iex> Fibonacci.get_number(0)
{:ok, 0}

elixir
iex> Fibonacci.get_number([0,1,100])
{:ok, [0, 1, 354224848179261915075]}

iex> Fibonacci.history()
[{0, 0}, {100, 354224848179261915075}, {1, 1}, {0, 0}]

