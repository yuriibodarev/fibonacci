defmodule FibTest do
  use ExUnit.Case

  describe "Finonacci can" do
    setup do
      start_supervised!(Fibonacci)

      :ok
    end

    test "return result for a single number" do
      assert {:ok, 0} == Fibonacci.get_number(0)
      assert {:ok, 354_224_848_179_261_915_075} == Fibonacci.get_number(100)
    end

    test "return result for a list of numbers" do
      assert {:ok, [0, 1, 354_224_848_179_261_915_075, 0]} == Fibonacci.get_number([0, 1, 100, 0])
    end

    test "return history" do
      Fibonacci.get_number(0)
      Fibonacci.get_number([1, 2])
      Fibonacci.get_number(100)
      Fibonacci.get_number([1, 2])

      assert [{0, 0}, {1, 1}, {2, 1}, {100, 354_224_848_179_261_915_075}, {1, 1}, {2, 1}] == Fibonacci.history()
    end
  end
end
