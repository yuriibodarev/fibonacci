defmodule Fibonacci.CalculatorTest do
  use ExUnit.Case, async: true

  alias Fibonacci.Calculator

  describe "Calculator can" do
    test "return starting numbers" do
      assert 0 == Calculator.calculate_number(0)
      assert 1 == Calculator.calculate_number(1)
      assert 1 == Calculator.calculate_number(2)
    end

    test "calculate Fibonacci numbers" do
      assert 354_224_848_179_261_915_075 == Calculator.calculate_number(100)
    end
  end
end
