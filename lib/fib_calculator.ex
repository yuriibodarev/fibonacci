defmodule Fibonacci.Calculator do
  @moduledoc """
  Fibonacci numbers calculator
  """

  def calculate_number(0), do: 0

  def calculate_number(1), do: 1

  def calculate_number(n) when n > 1 do
    do_calculate_number(0, 1, n)
  end

  defp do_calculate_number(prec, cur, 2), do: prec + cur

  defp do_calculate_number(prec, cur, n), do: do_calculate_number(cur, prec + cur, n - 1)
end
