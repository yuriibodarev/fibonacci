defmodule Fibonacci do
  use GenServer

  alias Fibonacci.Calculator

  # Server (callbacks)

  @impl true
  def init(_) do
    {:ok, cache} = Agent.start(fn -> %{} end)

    {:ok, {cache, []}}
  end

  @impl true
  def handle_call({:get_number, num}, _from, {cache, history}) do
    result = do_get_cache_number(num, cache)

    {:reply, result, {cache, history ++ [{num, result}]}}
  end

  @impl true
  def handle_call({:get_numbers, numbers}, _from, {cache, history}) do
    results =
      numbers
      |> Enum.map(&Task.async(fn -> {&1, do_get_cache_number(&1, cache)} end))
      |> Enum.map(&Task.await(&1))

    {:reply, Keyword.values(results), {cache, history ++ results}}
  end

  def handle_call(:history, _from, {cache, history}) do
    {:reply, history, {cache, history}}
  end

  defp do_get_cache_number(num, cache) do
    case Agent.get(cache, &Map.get(&1, num)) do
      nil ->
        result = Calculator.calculate_number(num)
        Agent.update(cache, &Map.put(&1, num, result))
        result

      result ->
        result
    end
  end

  # Client

  def start_link(_) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def get_number(num) when is_integer(num) and num >= 0 do
    res = GenServer.call(__MODULE__, {:get_number, num})
    {:ok, res}
  end

  def get_number(numbers) when is_list(numbers) do
    if Enum.all?(numbers, &(is_integer(&1) and &1 >= 0)) do
      res = GenServer.call(__MODULE__, {:get_numbers, numbers})
      {:ok, res}
    else
      {:error, :invalid_argument}
    end
  end

  def get_number(_), do: {:error, :invalid_argument}

  def history() do
    GenServer.call(__MODULE__, :history)
  end
end
